export class Nodo {
  id;
  papa;
  posicion;
  sillas;
  vecesVisitado;
  aristas = new Array();
  constructor(id,posicion,sillas) {
    this.id = id;
    this.posicion = posicion;
    this.sillas = sillas;
    this.vecesVisitado = 0;
  }

  setArista = (...nodos) => {
    nodos.forEach(nodo => {
      this.aristas.push(nodo);
    })
  }
}